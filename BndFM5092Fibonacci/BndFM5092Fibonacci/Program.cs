﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BndFM5092Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {

            Func<ulong, ulong> fibonacci = null;
            Func<ulong, ulong, ulong, ulong, ulong> fib_iter = null;

            fib_iter = (i1, i2, k, k_max) =>
                k < k_max ?
                fib_iter(i2, i1 + i2, k + 1, k_max) :
                i1 + i2;

            fibonacci = n =>
                n == 0 ? 0 :
                n == 1 ? 1 :
                fib_iter(0, 1, 1, n - 1);

            ulong prev = 0, curr = 0, ilast;
            string lead = "";
            for (ulong i = 0; true; i++)
            {
                ilast = i;
                prev = curr;
                curr = fibonacci(i);
                if (i < 10)
                    lead = "  ";
                else if (i < 100)
                    lead = " ";
                else
                    lead = "";
                Console.WriteLine(lead + i + ":  " + curr);
                if (prev > curr)
                    break;
            };
            Console.WriteLine("Overflow occurs at " + ilast);
            Console.WriteLine("fibonacci(" + (ilast - 1) + "):  " + fibonacci(ilast - 1));
            Console.WriteLine("fibonacci(" + ilast + "):  " + fibonacci(ilast));


            Console.ReadLine();
        }
    }
}
